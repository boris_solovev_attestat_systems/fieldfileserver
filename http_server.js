var express = require('express');
var app = express();
var path = require('path');
var formidable = require('formidable');
const cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');

function mkdir(dir) {
  var sep = path.sep;

  var segments = dir.split(sep);
  var current = '';
  var i = 0;

  while (i < segments.length) {
      if(segments[i][segments[i].length - 1] === ':') {
        current = segments[i];
        i++;
        continue;
      }

      current = current + sep + segments[i];

      try {
          fs.statSync(current);
      } catch (e) {
          fs.mkdirSync(current);
      }

      i++;
  }
}

app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use(bodyParser.json({limit: '150mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '150mb' }));
app.use(cors());

app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.post('/upload', function(req, res){
  var form = new formidable.IncomingForm();

  form.multiples = false;
  form.uploadDir = path.join(__dirname, '/uploads');

  form.on('field', function(name, value) {
    switch(name) {
      case 'terminal_serial':
        form.terminal_serial = value;
        break;
      case 'file_type':
        form.file_type = value;
        break;
      case 'channel_index':
        form.channel_index = value;
        break;
      default:
      break;
    }
  });  

  form.on('file', function(field, file) {
    form.original_file_name = file.name;
    form.content_file = file;
  });

  form.on('error', function(err) {
    console.log('An error has occured: \n' + err);
  });

  form.on('end', function() {
    const dirPath = path.join(
      form.uploadDir,
      form.terminal_serial,
      //form.file_type,
      //form.channel_index
    );

    mkdir(dirPath);
    
    fs.rename(form.content_file.path, path.join(dirPath, form.original_file_name.replace('.ready', '')));
    res.end(0);
  });

  form.parse(req);
});

var server = app.listen(9000, function(){
  console.log('Server listening on port ' + 9000);
});

module.exports = server;